#ifndef AOC_AOCFIFTH_HPP
#define AOC_AOCFIFTH_HPP

#include <iostream>
#include <limits>
#include <set>
#include <type_traits>

#include "Helper.hpp"

template < typename T >
class RangeC
{
  static_assert( std::is_arithmetic< T >::value, "T must be a numeric type" );

public:

  RangeC( T lower, T upper )
    : mLower( lower ),
      mUpper( upper )
  {}

  void setLowerHalf()
  {
    auto delta = mUpper - mLower;
    mUpper = delta / 2 + mLower;
  }

  void setUpperHalf()
  {
    auto delta = mUpper - mLower;
    mLower += delta / 2 + 1;
  }

  [[nodiscard]] T getLower() const
  {
    return mLower;
  }

  [[nodiscard]] T getUpper() const
  {
    return mUpper;
  }

private:

  T mLower;
  T mUpper;
};

struct Locator_t
{
  int32_t startpos;
  int32_t endpos;

  RangeC< int8_t > bounds;

  // changed this to static for clarity
  static int32_t getSeatId( const Locator_t& rowLocator, const Locator_t& columnLocator )
  {
    return rowLocator.bounds.getLower() * 8 + columnLocator.bounds.getLower();
  }
};

void locate( const std::string& s, Locator_t& locator )
{
  for ( auto i = locator.startpos; i < locator.endpos; ++i )
  {
    if ( s[ i ] == 'F' || s[ i ] == 'L' )
      locator.bounds.setLowerHalf();
    else
      locator.bounds.setUpperHalf();
  }
}

void runAOCFifth()
{
  int32_t maxSeatId = INT32_MIN;

  std::set< int32_t > seatIdSet;

  loadInputFile(
    "../inputs/aoc5.txt",
    [ & ]( const std::string& s ) -> bool
    {
      Locator_t rowLocator { 0, 7, { 0, 127 } };
      locate( s, rowLocator );

      Locator_t columnLocator { 7, 10, { 0, 7 } };
      locate( s, columnLocator );

      auto seatId = Locator_t::getSeatId( rowLocator, columnLocator );
      seatIdSet.insert( seatId );

      maxSeatId = std::max( maxSeatId, seatId );

      return true;
    } );

  std::cout << "max seat id: " << maxSeatId << std::endl;

  for ( int32_t i = 0; i < maxSeatId; ++i )
  {
    if ( seatIdSet.find( i ) == seatIdSet.end() )
      std::cout << "\t missing: " << i << std::endl;
  }

}

#endif //AOC_AOCFIFTH_HPP
