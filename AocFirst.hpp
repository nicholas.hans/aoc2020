#ifndef AOC_AOCFIRST_HPP
#define AOC_AOCFIRST_HPP

#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <stack>
#include <algorithm>
#include <set>

#include "Helper.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/***
 * The ExpenseReport is a wrapper around a stack that keeps the sum of the values in the stack
 */
class ExpenseReportC
{
public:
  ExpenseReportC() : mStackAddition( 0 ) {}

  /***
   * Only used for dumping the solution. It copies from one stack to another
   * so don't use it frequently (unless debugging)!
   */
  void dump()
  {
    std::stack< int32_t > tmp;

    int32_t sum = 0;
    int64_t mult = 1;

    auto counter = mExpenseStack.size();

    std::cout << "---------------------------------------------" << std::endl;

    while ( !mExpenseStack.empty() )
    {
      auto top = mExpenseStack.top();
      sum += top;
      mult *= top;
      std::cout << counter << ") " << top << std::endl;
      tmp.push( top );
      mExpenseStack.pop();
      --counter;
    }

    while ( !tmp.empty() )
    {
      mExpenseStack.push( tmp.top() );
      tmp.pop();
    }

    std::cout << "sum = " << sum << std::endl;
    std::cout << "mul = " << mult << std::endl;
  }

  void push( int32_t number )
  {
    mExpenseStack.push( number );
    mStackAddition += number;
  }

  void pop()
  {
    auto amount = mExpenseStack.top();
    mExpenseStack.pop();
    mStackAddition -= amount;
  }

  [[nodiscard]] size_t depth() const
  {
    return mExpenseStack.size();
  }

  [[nodiscard]] int32_t getSum() const
  {
    return mStackAddition;
  }

private:

  std::stack< int32_t > mExpenseStack;
  int32_t mStackAddition;
};

/***
 * Finds all matches
 * @param sortedVector a vector of numbers whose values are sorted
 * @param expenseReport @see ExpenseReportC
 * @param position current position in the sortedVector
 * @param maxDepth maximum amount of numbers to be used in calculating the target number
 * @param target the number that is trying to be reached
 */
void findMatches(
  std::vector< int32_t >& sortedVector,
  ExpenseReportC& expenseReport,
  int32_t position,
  int32_t maxDepth,
  int32_t target )
{
  // if we've reached our depth and the sum is correct then we've solved it once
  if ( expenseReport.getSum() == target && expenseReport.depth() == maxDepth )
  {
    // dump out the state of our ExpenseReport
    expenseReport.dump();
    return;
  }

  // because the vector is sorted, we can skip unnecessary calculations once our target has been exceeded
  if ( expenseReport.depth() < maxDepth && expenseReport.getSum() < target )
  {
    for ( ; position < sortedVector.size(); ++position )
    {
      // add the current value at this position
      expenseReport.push( sortedVector[ position ] );
      // look for any matches now that we've added to our report
      findMatches( sortedVector, expenseReport, position + 1, maxDepth, target );
      // remove the value that we added so we can add a new number in its place
      expenseReport.pop();
    }
  }
}

void runAOCFirst()
{
  // prepare our vector
  std::vector< int32_t > inputVector;

  // load integers from file into vector
  auto numberCount = loadInputFile< int32_t >(
    "../inputs/aoc1.txt",
    inputVector,
    []( const std::string& s ) -> int32_t { return std::stoi( s ); } );

  // sort the vector so we don't have to exhaust every possibility
  std::sort( inputVector.begin(), inputVector.end() );

  // solve for the following depths
  for ( int32_t i = 2; i < 5; ++i )
  {
    // prepare our ExpenseReport, which is our state
    ExpenseReportC expenseReport;
    findMatches( inputVector, expenseReport, 0, i, 2020 );
  }
}

#endif //AOC_AOCFIRST_HPP
