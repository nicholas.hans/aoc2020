#ifndef AOC_AOCSECOND_HPP
#define AOC_AOCSECOND_HPP

#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <stack>
#include <algorithm>
#include <set>

#include "Helper.hpp"

/***
 * The specific pwd policy to be verified
 */
struct PasswordPolicy_t
{
  PasswordPolicy_t()
    : lowerbound( 0 ),
      upperbound( 0 )
  {}

  int32_t lowerbound;
  int32_t upperbound;
  std::string policy;
  std::string password;
};

/***
 * Verifies how many policies meet the conditions set by the org's pwd policy
 * @param pwdPolicyVector the vector of policies
 * @param verifyPolicyValidity the callback method that performs the validation
 * @return number of verified policies
 */
int32_t getValidPolicyCount(
  std::vector< PasswordPolicy_t >& pwdPolicyVector,
  const std::function< bool( const PasswordPolicy_t& ) >& verifyPolicyValidity )
{
  int32_t validCounts = 0;

  for ( const auto& policy : pwdPolicyVector )
  {
    if ( verifyPolicyValidity( policy ) )
      ++validCounts;
  }

  return validCounts;
}

void runAOCSecond()
{
  std::vector< PasswordPolicy_t > pwdPolicyVector;

  enum E_PolicyParts { BOUNDS = 0, POLICY = 1, PWD = 2 };

  // parse the contents of the file into Password Policies
  auto numberCount = loadInputFile< PasswordPolicy_t >(
    "../inputs/aoc2.txt",
    pwdPolicyVector,
    []( const std::string& s ) -> PasswordPolicy_t
    {
      PasswordPolicy_t result;

      auto splitBySpace = Split< std::vector< std::string > >( s );
      result.policy = splitBySpace[ E_PolicyParts::POLICY ].substr( 0, splitBySpace[ E_PolicyParts::POLICY ].size() - 1 );
      result.password = splitBySpace[ E_PolicyParts::PWD ];

      auto splitByHyphen = Split< std::vector< std::string > >( splitBySpace[ E_PolicyParts::BOUNDS ], '-' );
      result.lowerbound = std::stoi( splitByHyphen[ 0 ] );
      result.upperbound = std::stoi( splitByHyphen[ 1 ] );

      return result;
    } );

  // get the count of valid policies according to the first understanding of the policy
  auto validCount = getValidPolicyCount(
    pwdPolicyVector,
    []( const PasswordPolicy_t &policy ) -> bool
    {
      auto occurrences = std::count_if(
        policy.password.begin(),
        policy.password.end(),
        [ &policy ]( const char c ) -> bool
        { return ( c == policy.policy.at( 0 )); } );

      return ( occurrences >= policy.lowerbound && occurrences <= policy.upperbound );
    } );

  // get the count of valid policies according to the second understanding of the policy
  auto enhancedValidCount = getValidPolicyCount(
    pwdPolicyVector,
    []( const PasswordPolicy_t &policy ) -> bool
    {
      // XOR boolstate => ( T & F ) | ( F & T ) only
      bool boolstate = false;

      if ( policy.password[ policy.lowerbound - 1 ] == policy.policy[ 0 ] )
        boolstate = !boolstate;

      if ( policy.password[ policy.upperbound - 1 ] == policy.policy[ 0 ] )
        boolstate = !boolstate;

      return boolstate;
    } );

  std::cout << "Part I: " << validCount
            << std::endl
            << "Part II: " << enhancedValidCount
            << std::endl;
}

#endif //AOC_AOCSECOND_HPP
