#ifndef AOC_HELPER_HPP
#define AOC_HELPER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>
#include <stack>
#include <sstream>
#include <algorithm>
#include <regex>

template < typename T >
size_t loadInputFile( const std::string& fileIn, std::vector< T >& inputVector, const std::function< T( const std::string& ) >& convert )
{
  size_t counter = 0;
  std::ifstream fileStream( fileIn );
  if ( fileStream.is_open() )
  {
    std::string line {};
    while ( std::getline( fileStream, line ) )
    {
      inputVector.emplace_back( convert( line ) );
      ++counter;
    }
  }

  return counter;
}

/***
 * Used for reading a line and processing it prior to moving on to the next
 * @param fileIn
 * @param onLineRead
 * @return number of lines read
 */
size_t loadInputFile( const std::string& fileIn, const std::function< bool( const std::string& ) >& onLineRead )
{
  size_t counter = 0;
  std::ifstream fileStream( fileIn );
  if ( fileStream.is_open() )
  {
    std::string line {};
    while ( std::getline( fileStream, line ) )
    {
      if ( !onLineRead( line ) )
        break;

      ++counter;
    }
  }

  return counter;
}

template < class TContainer >
TContainer Split( const std::string& str, char delim = ' ' )
{
  TContainer container;
  std::stringstream ss( str );
  std::string token;

  while ( std::getline( ss, token, delim ) )
    container.push_back( token );

  return container;
}

std::vector< std::string > Split( std::string& s, std::regex& rgxPattern )
{
  std::vector< std::string > result;
  std::regex_token_iterator< std::string::iterator > rbegin( s.begin(), s.end(), rgxPattern, -1 );
  std::regex_token_iterator< std::string::iterator > rend;

  while ( rbegin != rend )
  {
    result.push_back( *rbegin );
    rbegin++;
  }

  return result;
}

#endif //AOC_HELPER_HPP
