#ifndef AOC_AOCNINTH_HPP
#define AOC_AOCNINTH_HPP

#include <iostream>
#include <type_traits>
#include <queue>
#include <set>

#include "Helper.hpp"

/***
 * A bag is a dequeue that keeps the sum of its values
 * @tparam T numeric type
 */
template < typename T >
class BagC
{
  static_assert( std::is_arithmetic< T >::value, "T must be an arithemtic number" );

public:

  BagC()
    : mSum( 0 )
  {}

  virtual void add( T item )
  {
    mContainer.push_back( item );
    mSum += item;
  }

  virtual T pop()
  {
    auto front = mContainer.front();
    mContainer.pop_front();
    mSum -= front;
    return front;
  }

  /***
   * @return the number of items in the window
   */
  [[nodiscard]] virtual size_t size() const
  {
    return mContainer.size();
  }

  /***
   * @return the sum of the numbers in the current window
   */
  [[nodiscard]] virtual T getSum() const
  {
    return mSum;
  }

  void clear()
  {
    mContainer.clear();
    mSum = 0;
  }

  // there's probably a better way so that your min and max ranges are kept track of in real time
  [[nodiscard]] std::tuple< T, T > getMinMaxRange() const
  {
    T max;
    T min;
    max = min = mContainer[ 0 ];

    for ( const auto& item : mContainer )
    {
      min = std::min( item, min );
      max = std::max( item, max );
    }
    return std::tuple< T, T >( min, max );
  }

protected:

  std::deque< T > mContainer;

private:

  T mSum;
};

/***
 * x - deque[ a ] = b and then b is looked up in the set. it should reduce runtime to O( n )
 * @tparam TWindowSize size of the scrolling window
 * @tparam T type of integer
 */
template < uint32_t TWindowSize, typename T >
class ScrollingWindowC : private BagC< T >
{
public:

  void add( T item ) override
  {
    // once the preamble is full begin adjusting window
    if ( this->mContainer.size() == TWindowSize )
    {
      auto front = BagC< T >::pop();
      mDataSet.erase( front );
    }

    BagC< T >::add( item );
    mDataSet.insert( item );
  }

  /***
   * Retrieves all the pairs that sum of to numToVerify
   * @param numToVerify the number to verify (x)
   * @return vector of pairs that add up to numToVerify
   */
  std::vector< std::tuple< T, T > > getPairs( T numToVerify )
  {
    std::vector< std::tuple< T, T > > result;

    for ( const auto& a : this->mContainer )
    {
      // b = x - a
      auto b = numToVerify - a;

      // find b in the set
      if ( mDataSet.find( b ) != mDataSet.end() )
        result.push_back( { a, b } );
    }

    return result;
  }

  /***
   * @return the number of items in the window
   */
  [[nodiscard]] size_t size() const override
  {
    return this->mContainer.size();
  }

  /***
   * @return the sum of the numbers in the current window
   */
  [[nodiscard]] T getSum() const override
  {
    return BagC< T >::getSum();
  }

private:

  std::set< T > mDataSet;
};

// this is for contiguous sum so we don't need to cache results but this is still not the most elegant solution
void findSubsetSum( uint64_t target, std::vector< uint64_t >& numbers )
{
  BagC< uint64_t > subsetBag;

  for ( auto i = 0; i < numbers.size() - 1; ++i )
  {
    subsetBag.add( numbers[ i ] );

    while ( subsetBag.getSum() > target )
      subsetBag.pop();

    if ( subsetBag.getSum() == target )
    {
      auto range = subsetBag.getMinMaxRange();
      auto sum = std::get< 0 >( range ) + std::get< 1 >( range );
      std::cout << std::get< 0 >( range ) << " + " << std::get< 1 >( range ) << " = " << sum << std::endl;
      break;
    }
  }
}

void runAOCNinth()
{
  const int32_t windowsize = 25;
  ScrollingWindowC< windowsize, uint64_t > bag;
  std::vector< uint64_t > numbers;

  uint64_t firstFailure = 0;

  loadInputFile(
    "../inputs/aoc9.txt",
    [ & ]( const std::string& s ) -> bool
    {
      if ( s.empty() )
        return false;

      uint64_t currentNumber = std::stoul( s );
      numbers.push_back( currentNumber );

      // is the preamble met?
      if ( bag.size() == windowsize )
      {
        auto pairs = bag.getPairs( currentNumber );

        if ( pairs.empty() )
          firstFailure = currentNumber;
      }

      bag.add( currentNumber );
      return true;
    } );

  std::cout << "Failed: " << firstFailure << std::endl;
  findSubsetSum( firstFailure, numbers );

}

#endif //AOC_AOCNINTH_HPP
