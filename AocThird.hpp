#ifndef AOC_AOCTHIRD_HPP
#define AOC_AOCTHIRD_HPP

#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <stack>
#include <algorithm>
#include <set>

#include "Helper.hpp"

/***
 * Wrapper around set that gives us an infinite plane of tree growth
 * so we can find whether an arbitrary position is a tree or not
 */
class TreePatternC
{

public:

  /***
   * The size of field, i.e., the number of chars in a line
   * @param fieldsize
   */
  static void setFieldSize( int32_t fieldsize )
  {
    mFieldSize = fieldsize;
  }

  /***
   * sets a tree in a certain position
   * @param position of the tree
   */
  void setTreePosition( int32_t position )
  {
    mTrees.insert( position );
  }

  /***
   *
   * @param position of inquiry
   * @return true if the position is a tree, otherwise false
   */
  bool isTree( int32_t position )
  {
    // because the map can repeat, we need to use mod
    return mTrees.find( position % mFieldSize ) != mTrees.end();
  }

private:

  static inline int32_t mFieldSize = 0;
  std::set< int32_t > mTrees;
};

/***
 * Handles what our next step is going both down and right
 */
class TravelPattern
{
  enum E_Coords
  {
    RIGHT = 0,
    DOWN = 1
  };

public:

  explicit TravelPattern( std::tuple< int32_t, int32_t >&& travelPattern )
    : mTravelPattern( travelPattern ),
      mTravelAbsPosition( 0 ),
      mCallsMade( 0 )
  {}

  /***
   * Gets the next position in a line based on the DOWN and RIGHT options (if the line is to be skipped then -1)
   * @return >= 0 if the next position is valid, otherwise -1
   */
  int32_t next()
  {
    int32_t result = -1;

    if ( mCallsMade % std::get< E_Coords::DOWN >( mTravelPattern ) == 0 )
    {
      result = mTravelAbsPosition;
      mTravelAbsPosition += std::get< E_Coords::RIGHT >( mTravelPattern );
    }

    ++mCallsMade;

    return result;
  }

private:
  std::tuple< int32_t, int32_t > mTravelPattern;
  int32_t mTravelAbsPosition;
  int32_t mCallsMade;
};

void ParseTreeLine( const std::string& s, TreePatternC& treePattern )
{
  for ( int32_t i = 0; i < s.size(); ++i )
  {
    if ( s[ i ] == '#' )
      treePattern.setTreePosition( i );
  }
}

int32_t getTreeEncounters( TravelPattern& travelPattern )
{
  int32_t treeEncounters = 0;

  loadInputFile(
    "../inputs/aoc3.txt",
    [ & ]( const std::string& s ) -> bool
    {
      // process the tree pattern
      TreePatternC treePattern;
      ParseTreeLine( s, treePattern );

      auto next = travelPattern.next();

      // check whether the next movement is a tree
      if ( treePattern.isTree( next ) )
        ++treeEncounters;

      return true;
    } );

  return treeEncounters;
}

void runAOCThird()
{
  TreePatternC::setFieldSize( 31 );
  std::vector< TravelPattern > travelPatterns
    {
      // 1 = 79
      // 3 = 234
      // 5 = 72
      // 7 = 91
      // 1, 2 = 48

      TravelPattern( { 1, 1 } ),
      TravelPattern( { 3, 1 } ),
      TravelPattern( { 5, 1 } ),
      TravelPattern( { 7, 1 } ),
      TravelPattern( { 1, 2 } )
    };

  int64_t encountersMultiplied = 1;

  for ( auto& travelPattern : travelPatterns )
    encountersMultiplied *= getTreeEncounters( travelPattern );

  std::cout << encountersMultiplied << std::endl;
}

#endif //AOC_AOCTHIRD_HPP
