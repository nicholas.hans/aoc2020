#ifndef AOC_AOCFOURTH_HPP
#define AOC_AOCFOURTH_HPP

#include <iostream>
#include <map>
#include <regex>
#include <set>
#include <fstream>

#include "Helper.hpp"

/***
 * Primarily made this for debugging because i thought my verifications weren't working properly.
 * It tracks the value and indicates whether the value successfully meets the criteria
 */
struct ValueValid
{
  ValueValid()
    : isValid( false )
  {}

  explicit ValueValid( const std::string& v )
    : value( v ),
      isValid( false )
  {}

  ValueValid( const std::string& v, bool validState )
    : value( v ),
      isValid( validState )
  {}

  void setValidState( bool validstate )
  {
    isValid = validstate;
  }

  std::string value;
  bool isValid;
};

/***
 * Policy class for validating the first part
 */
class SimpleValidationPolicy
{
protected:
  [[nodiscard]] bool isValid( int32_t numberOfFields, std::map< std::string, ValueValid >& datamap ) const
  {
    return ( datamap.size() == numberOfFields ||
             ( datamap.size() == numberOfFields - 1 && datamap.find( "cid" ) == datamap.end() ) );
  }
};

/***
 * Policy class for validating the second part
 */
class ComplexValidationPolicy
{

protected:
  [[nodiscard]] bool isValid( int32_t numberOfFields, std::map< std::string, ValueValid >& datamap ) const
  {
    // same as in simple
    bool result = ( datamap.size() == numberOfFields ||
                    ( datamap.size() == numberOfFields - 1 && datamap.find( "cid" ) == datamap.end() ) );

    for ( const auto& kvpair : datamap )
    {
      // call the validation function and store the result
      auto validstate = mRules[ kvpair.first ]( kvpair.second.value );
      // maintain the correct valid|invalid state: we check all here so each field can be flagged for issues
      result = result && ( mRules.find( kvpair.first ) != mRules.end() ) && validstate;
      // store the result so we can spend precious hours debugging it
      // only to realize that we didn't read all the rules
      datamap.at( kvpair.first ).isValid = validstate;
    }

    return result;
  }

private:

  // basic range validation
  static bool isValidRange( const std::string& s, int32_t lower, int32_t upper )
  {
    bool result;

    try
    {
      auto v = std::stoi( s );
      result = ( v >= lower && v <= upper );
    }
    catch ( std::invalid_argument& ex ) { result = false; }

    return result;
  }

  static bool isHCLValid( const std::string& s )
  {
    static std::regex rgx( "^#[0-9a-f]{6}$" );
    return s[ 0 ] == '#' && std::regex_match( s, rgx );
  }

  static bool isPIDValid( const std::string& s )
  {
    static std::regex rgx( "^[0-9]{9}$" );
    return std::regex_match( s, rgx );
  }

  static bool isECLValid( const std::string& s )
  {
    static std::set< std::string > eyeColors
      {
        "amb", "blu", "brn", "gry", "grn", "hzl", "oth"
      };

    return eyeColors.find( s ) != eyeColors.end();
  }

  static bool isHGTValid( const std::string& s )
  {
    bool result = false;

    if ( s.size() > 2 )
    {
      auto unit = s.substr( s.size() - 2, 2 );
      auto value = std::stoi( s.substr( 0, s.size() - 2 ) );

      if ( unit == "cm" )
        result = ( value >= 150 && value <= 193 );
      else if ( unit == "in" )
        result = ( value >= 59 && value <= 76 );
    }

    return result;
  }

private:

  // map which fields go with which validation type
  static inline std::map< std::string, std::function< bool( const std::string& ) > > mRules
    {
      { "byr", []( const std::string& s ) -> bool { return isValidRange( s, 1920, 2002 ); } },
      { "iyr", []( const std::string& s ) -> bool { return isValidRange( s, 2010, 2020 ); } },
      { "eyr", []( const std::string& s ) -> bool { return isValidRange( s, 2020, 2030 ); } },

      { "hgt", ComplexValidationPolicy::isHGTValid },
      { "hcl", ComplexValidationPolicy::isHCLValid },
      { "ecl", ComplexValidationPolicy::isECLValid },

      { "pid", ComplexValidationPolicy::isPIDValid },
      { "cid", []( const std::string& s ) -> bool { return true; } },
    };
};

/***
 * The passport data container
 * @tparam NumberOfFields the number of fields in a passport (even we don't use them all)
 * @tparam TValidationPolicy the validation policy we want to use on a passport
 */
template < int NumberOfFields, typename TValidationPolicy >
class PassportC : private TValidationPolicy
{
public:

  void add( const std::string& keyValuePair )
  {
    auto pos = keyValuePair.find( ':' );
    add(
      keyValuePair.substr( 0, pos ),
      keyValuePair.substr( pos + 1,
                           keyValuePair.size() - ( pos + 1 ) ) );
  }

  void add( const std::string& field, const std::string& value )
  {
    if ( mDataMap.find( field ) == mDataMap.end() )
      mDataMap.insert( { field, ValueValid( value ) } );
  }

  [[nodiscard]] size_t size() const
  {
    return mDataMap.size();
  }

  [[nodiscard]] bool isValid()
  {
    return TValidationPolicy::isValid( NumberOfFields, mDataMap );
  }

  /***
   * dumps out the contents
   * @param passportId a counter
   * @param ostream the output stream
   */
  void writeToFile( int32_t passportId, std::ostream& ostream )
  {
    auto validstate = isValid();

    ostream << passportId << ". [" << validstate << "]" << std::endl;

    for ( const auto &kvpair : mDataMap )
    {
      ostream << passportId
              << ". \t(" << size()
              << ") \t" << kvpair.first
              << " [" << kvpair.second.isValid << "] -> \t"
              << ": \t"
              << kvpair.second.value
              << std::endl;
    }
  }

private:
  std::map< std::string, ValueValid > mDataMap;
};

void runAOCFourth()
{
  const int32_t expectedFieldCount = 8;
  typedef PassportC< expectedFieldCount, ComplexValidationPolicy > Passport_t;
  std::unique_ptr< Passport_t > ptrCurrentPassport;

  int32_t validPassports = 0;
  int32_t totalCount = 0;

  std::ofstream outfile;
  outfile.open( "../inputs/aoc4_out.txt" );

  loadInputFile(
    "../inputs/aoc4.txt",
    [ & ]( const std::string& s ) -> bool
    {
      // start of a record
      if ( ptrCurrentPassport == nullptr )
        ptrCurrentPassport = std::make_unique< Passport_t >();

      // is this the end of a record?
      if ( s.empty() )
      {
        // check whether the current passport is valid
        // ptrCurrentPassport
        if ( ptrCurrentPassport->isValid() )
          ++validPassports;

        // log it for debugging purposes
        ptrCurrentPassport->writeToFile( totalCount, outfile );

        // delete the current pointer and then set it to nullptr
        ptrCurrentPassport.reset();
        ++totalCount;
      }
      else
      {
        // parse line
        // add content to the current passport
        auto splitStringVector = Split< std::vector< std::string > >( s );
        for ( const auto& itemInVector : splitStringVector )
          ptrCurrentPassport->add( itemInVector );
      }

      return true;
    } );

  // make sure we capture the final passport if one exists
  if ( ptrCurrentPassport != nullptr )
  {
    ptrCurrentPassport->writeToFile( totalCount, outfile );

    if ( ptrCurrentPassport->isValid() )
      ++validPassports;
  }

  outfile.flush();
  outfile.close();

  std::cout << "Valid passports: " << validPassports << std::endl;
}

#endif //AOC_AOCFOURTH_HPP
