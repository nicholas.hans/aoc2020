#ifndef AOC_AOCSIXTH_HPP
#define AOC_AOCSIXTH_HPP

#include <map>

#include "Helper.hpp"

class CustomsFormC
{
public:

  CustomsFormC()
    : mPersons( 0 )
  {}

  void add( const std::string& s )
  {
    ++mPersons;

    for ( const auto& c : s )
    {
      if ( mAnswerSet.find( c ) == mAnswerSet.end() )
        mAnswerSet.insert( { c, 1 } );
      else
        mAnswerSet[ c ]++;
    }
  }

  [[nodiscard]] int32_t getFullIntersection() const
  {
    int32_t result = 0;

    for ( const auto& kvpair : mAnswerSet )
    {
      if ( kvpair.second == mPersons )
        ++result;
    }

    return result;
  }

  [[nodiscard]] size_t size() const
  {
    return mAnswerSet.size();
  }

private:
  int32_t mPersons;
  std::map< char, int32_t > mAnswerSet;
};

void runAOCSixth()
{
  std::unique_ptr< CustomsFormC > ptrCustomsForm;

  int32_t totalCount = 0;
  int32_t totalFullIntersections = 0;

  loadInputFile(
    "../inputs/aoc6.txt",
    [ & ]( const std::string& s ) -> bool
    {
      if ( ptrCustomsForm == nullptr )
        ptrCustomsForm = std::make_unique< CustomsFormC >();

      if ( s.empty() )
      {
        totalCount += ptrCustomsForm->size();
        totalFullIntersections += ptrCustomsForm->getFullIntersection();
        // end of record
        ptrCustomsForm.reset();
      }
      else
        ptrCustomsForm->add( s );

      return true;
    } );

  if ( ptrCustomsForm != nullptr )
  {
    totalCount += ptrCustomsForm->size();
    totalFullIntersections += ptrCustomsForm->getFullIntersection();
  }

  std::cout << totalCount << ": " << totalFullIntersections << std::endl;
}

#endif //AOC_AOCSIXTH_HPP
